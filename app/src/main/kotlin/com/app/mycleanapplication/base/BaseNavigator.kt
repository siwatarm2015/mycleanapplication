package com.app.mycleanapplication.base

/**
 * Created by chonlakan.s on 11:19 9/18/18
 */
interface BaseNavigator {

    fun clearKeyboard()

    fun goBackToPrevious()
}
