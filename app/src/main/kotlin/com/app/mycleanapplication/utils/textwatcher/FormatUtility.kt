package com.app.mycleanapplication.utils.textwatcher

/**
 * Created by chonlakan.joke on 2019-10-28. At smart-preventive-health-care
 */

object FormatUtility {
    const val SEPARATE = " "
    const val TEXT_FORMAT_NUMBER_PROFILE_STAFF = "# #####"
    const val TEXT_FORMAT_PHONE = "### ### ####"
    const val TEXT_FORMAT_CITIZEN = "# #### ##### ## #"

    fun applyStringPattern(text: String, format: String): String {
        val pattern = StringBuilder() // ex. pattern "(\\d{3})(\\d{3})(\\d+)"
        val replacement = StringBuilder() // ex. replacement "$1-$2-$3"
        val formats = format.split(SEPARATE.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        for (i in formats.indices) {
            pattern.append("(\\d{").append(formats[i].length).append("})")
            if (i == 0) {
                replacement.append("$").append(i + 1)
            } else {
                replacement.append("$SEPARATE$").append(i + 1)
            }
        }
        return text.replaceFirst(pattern.toString().toRegex(), replacement.toString())
    }
}
