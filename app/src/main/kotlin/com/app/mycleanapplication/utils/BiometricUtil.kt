package com.app.mycleanapplication.utils

import android.content.Context
import androidx.biometric.BiometricManager
import android.os.Build

/**
 * Created by chonlakan.joke on 2019-11-09. At smart-preventive-health-care
 */
object BiometricUtil {

    fun isHardwareAvailable(context: Context): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val bm = BiometricManager.from(context)
            val canAuthenticate = bm.canAuthenticate()
            !(canAuthenticate == BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE || canAuthenticate == BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE)
        } else {
            false
        }
    }

    fun hasBiometricEnrolled(context: Context): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            BiometricManager.from(context)
            val bm = BiometricManager.from(context)
            val canAuthenticate = bm.canAuthenticate()
            (canAuthenticate == BiometricManager.BIOMETRIC_SUCCESS)
        } else {
            false
        }
    }
}
