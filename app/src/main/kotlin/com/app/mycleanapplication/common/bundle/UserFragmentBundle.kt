package com.app.mycleanapplication.common.bundle

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserFragmentBundle(
    var id: String = ""
) : Parcelable
