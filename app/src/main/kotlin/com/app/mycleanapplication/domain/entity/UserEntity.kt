package com.app.mycleanapplication.domain.entity

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "user_table")
@Parcelize
data class UserEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    @Embedded val address: Address = Address(),
    @Embedded val company: Company = Company(),
    val userId: String = "",
    val email: String = "",
    val name: String = "",
    val phone: String = "",
    val username: String = "",
    val website: String = ""
) : Parcelable {
    @Parcelize
    data class Address(
        val city: String = "",
        @Embedded val geo: Geo = Geo(),
        val street: String = "",
        val suite: String = "",
        val zipcode: String = ""
    ) : Parcelable {
        @Parcelize
        data class Geo(
            val lat: String = "",
            val lng: String = ""
        ): Parcelable
    }

    @Parcelize
    data class Company(
        val bs: String = "",
        val catchPhrase: String = "",
        val companyName: String = ""
    ) : Parcelable
}