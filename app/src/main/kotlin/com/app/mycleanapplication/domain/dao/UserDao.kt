package com.app.mycleanapplication.domain.dao

import androidx.room.*
import com.app.mycleanapplication.domain.model.users.UsersResponse
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(users: List<UsersResponse>)

    @Insert
    fun insertUser(userEntityEntity: UsersResponse)

    @Update
    fun updateUser(userEntityEntity: UsersResponse)

    @Delete
    fun deleteUser(userEntityEntity: UsersResponse)

    @Query("SELECT * FROM user_table")
    fun getUserListFromCache(): Flow<MutableList<UsersResponse>>

    @Query("SELECT * FROM user_table WHERE user_table.userId = :userId")
    fun getUserById(userId: String): Flow<UsersResponse>

    @Query("DELETE FROM user_table")
    fun deleteTable()
}