package com.app.mycleanapplication.domain.usecase.home

import com.app.mycleanapplication.data.repository.MainRepository
import com.app.mycleanapplication.domain.model.request.GetHomeRequest

class GetUserUseCase(private val mainRepository: MainRepository) {

    suspend operator fun invoke() =
        mainRepository.getUsers()

    suspend operator fun invoke(id: String) =
        mainRepository.getUsersById(id)

//    suspend fun getHomeLocal(getHomeRequest: GetHomeRequest) =
//        mainRepository.getHomeLocal(getHomeRequest)
}