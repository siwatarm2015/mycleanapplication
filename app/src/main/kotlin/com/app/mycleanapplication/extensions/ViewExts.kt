package com.app.mycleanapplication.extensions

import android.view.View
import com.app.mycleanapplication.utils.SafeClickListener
import com.google.android.material.snackbar.Snackbar

fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
    val safeClickListener = SafeClickListener {
        onSafeClick(it)
    }
    setOnClickListener(safeClickListener)
}

fun View.showSnackBar(){
    Snackbar.make(this, "Replace with your own action", Snackbar.LENGTH_LONG)
        .setAction("Action", null).show()
}
