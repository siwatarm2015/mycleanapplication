package com.app.mycleanapplication.presentation.ui.home

import com.app.mycleanapplication.base.BaseNavigator


/**
 * Created by chonlakan.s on 10:53 2018-12-19
 */
interface HomeNavigator : BaseNavigator {

    fun navigateToDetail(id: String)
}
