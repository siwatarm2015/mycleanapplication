package com.app.mycleanapplication.presentation.ui

import com.app.mycleanapplication.base.BaseNavigator


/**
 * Created by chonlakan.s on 10:15 2018-12-19
 */
interface MainNavigator : BaseNavigator {
    fun navigateToNotificationFragment()
}
