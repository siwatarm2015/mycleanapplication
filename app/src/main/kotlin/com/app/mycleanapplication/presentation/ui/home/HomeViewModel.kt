package com.app.mycleanapplication.presentation.ui.home

import androidx.lifecycle.viewModelScope
import com.app.mycleanapplication.base.BaseViewModel
import com.app.mycleanapplication.domain.Result
import com.app.mycleanapplication.domain.Result.Failure
import com.app.mycleanapplication.domain.Result.Loading
import com.app.mycleanapplication.domain.model.users.UsersResponse
import com.app.mycleanapplication.domain.usecase.home.GetUserUseCase
import com.app.mycleanapplication.exception.NoConnectivityException
import com.app.mycleanapplication.utils.retryIO
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

/**
 * Created by chonlakan.s on 10:13 2018-12-19
 */

class HomeViewModel(
    private val getUserUseCase: GetUserUseCase
) : BaseViewModel<HomeNavigator>() {

    private val _usersResponse =
        MutableStateFlow<Result<MutableList<UsersResponse>>>(Loading(isLoad = true))
    val usersResponse: StateFlow<Result<MutableList<UsersResponse>>> = _usersResponse

    private val _localResponse =
        MutableStateFlow<Result<UsersResponse>>(Loading(isLoad = true))
    val localResponse: StateFlow<Result<UsersResponse>> = _localResponse

//    private val actionSender = ConflatedBroadcastChannel<Result<MutableList<UsersResponse>>>()
//    val actionReceiver = actionSender.asFlow()

//    private val _mainResponse = MutableLiveData<Result<BaseResponse<Nothing>>>()
//    val mainResponse: LiveData<Result<BaseResponse<Nothing>>> = _mainResponse

    fun getUsers() {
        viewModelScope.launch {
            try {
                retryIO(times = 3) {
                    getUserUseCase().onStart {
//                        actionSender.send(Loading(true))
                        _usersResponse.value = Loading(true)
                    }.collect {
//                        actionSender.send(it)
                        _usersResponse.value = it
                    }
                }
            } catch (e: NoConnectivityException) {
                _usersResponse.value = Failure(e.message)
            } catch (e: Exception) {
                _usersResponse.value = Failure(e.message)
            }
        }
    }

//    fun getHomeLocal(getHomeRequest: GetHomeRequest) {
//        viewModelScope.launch {
//            try {
//                retryIO(times = 3) {
//                    getHomeUseCase.getHomeLocal(getHomeRequest).onStart {
//                        _localResponse.value = Loading(true)
//                    }.collect {
//                        _localResponse.value = it
//                    }
//                }
//            } catch (e: NoConnectivityException) {
//                _localResponse.value = Failure(e.message)
//            } catch (e: Exception) {
//                _localResponse.value = Failure(e.message)
//            }
//        }
//    }
}
