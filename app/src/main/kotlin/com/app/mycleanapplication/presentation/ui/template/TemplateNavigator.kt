package com.app.mycleanapplication.presentation.ui.template

import com.app.mycleanapplication.base.BaseNavigator


/**
 * Created by chonlakan.s on 10:53 2018-12-19
 */
interface TemplateNavigator : BaseNavigator {

    fun navigateToRegisterIDCardFragment()
}
