package com.app.mycleanapplication.presentation.ui

import androidx.lifecycle.viewModelScope
import com.app.mycleanapplication.base.BaseViewModel
import com.app.mycleanapplication.domain.BaseResponse
import com.app.mycleanapplication.domain.Result
import com.app.mycleanapplication.domain.Result.Failure
import com.app.mycleanapplication.domain.Result.Loading
import com.app.mycleanapplication.domain.model.request.GetHomeRequest
import com.app.mycleanapplication.domain.usecase.home.GetUserUseCase
import com.app.mycleanapplication.exception.NoConnectivityException
import com.app.mycleanapplication.utils.retryIO
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class MainViewModel(private val getUserUseCase: GetUserUseCase) : BaseViewModel<MainNavigator>() {

    private val _mainResponse =
        MutableStateFlow<Result<BaseResponse<Nothing>>>(Loading(isLoad = true))
    val mainResponse: StateFlow<Result<BaseResponse<Nothing>>> = _mainResponse

    fun getHome(getHomeRequest: GetHomeRequest) {
        viewModelScope.launch {
            try {
                retryIO(times = 3) {
                    getUserUseCase().onStart {
                        _mainResponse.value = Loading(true)
                    }.collect {
//                        _mainResponse.value = it
                    }
                }
            } catch (e: NoConnectivityException) {
                _mainResponse.value = Failure(e.message)
            } catch (e: Exception) {
                _mainResponse.value = Failure(e.message)
            }
        }
    }

}