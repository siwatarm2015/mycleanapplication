package com.app.mycleanapplication.presentation.ui.detail

import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionInflater
import com.app.mycleanapplication.BR
import com.app.mycleanapplication.R
import com.app.mycleanapplication.base.BaseFragment
import com.app.mycleanapplication.common.bundle.UserFragmentBundle
import com.app.mycleanapplication.databinding.FragmentHomeBinding
import com.app.mycleanapplication.databinding.FragmentUserBinding
import com.app.mycleanapplication.domain.Result.*
import com.app.mycleanapplication.domain.model.TemplateModel
import com.app.mycleanapplication.domain.model.users.UsersResponse
import com.app.mycleanapplication.presentation.ui.home.HomeViewModel
import com.app.mycleanapplication.presentation.ui.home.adapter.UsersAdapter
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class UserDetailFragment :
    BaseFragment<FragmentUserBinding, UserViewModel>(),
    UserNavigator {

    private val viewModels: UserViewModel by viewModel()

    override fun getViewModel(): UserViewModel = viewModels

    override fun getLayoutId(): Int = R.layout.fragment_user

    override fun getBindingVariable(): Int = BR.model

    private var mArguments: UserFragmentBundle? = null

    private lateinit var usersAdapter: UsersAdapter

    private var usersListResponse = ArrayList<UsersResponse>()

    private var isInit = false

    override fun onResume() {
        super.onResume()
//        viewModels.getUsers()
    }

    override fun updateInstant(savedInstanceState: Bundle?) {
        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        // Receiver argument
        mArguments = args<UserDetailFragmentArgs>().userFragmentBundle.apply {
            viewModels.getUsersById(this.id)
        }
    }

    override fun observerLiveData() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModels.usersResponse.collect { result ->
                when (result) {
                    is Success -> {
                        toast(result.data?.name ?: "")
                    }
                    is Failure -> {
                    }
                    is Loading -> {
                    }
                }
            }
        }
    }

    override fun updateListener() {
//        btnBack.clickWithDebounce {
//            goBackToPrevious()
//        }
//        binding?.data = mArguments
    }

    override fun updateUI(savedInstanceState: Bundle?) {
        viewModels.setNavigator(this)
        binding?.viewModel = viewModels
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun clearKeyboard() {
    }

    override fun onBackPressActivityListener() {
        goBackToPrevious()
//        navController.popBackStack()
    }

    override fun goBackToPrevious() {
        navController.popBackStack()
    }

}
