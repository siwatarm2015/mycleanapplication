package com.app.mycleanapplication.presentation.ui.home

import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionInflater
import com.app.mycleanapplication.BR
import com.app.mycleanapplication.R
import com.app.mycleanapplication.base.BaseFragment
import com.app.mycleanapplication.common.bundle.UserFragmentBundle
import com.app.mycleanapplication.databinding.FragmentHomeBinding
import com.app.mycleanapplication.domain.Result.*
import com.app.mycleanapplication.domain.model.TemplateModel
import com.app.mycleanapplication.domain.model.users.UsersResponse
import com.app.mycleanapplication.presentation.ui.home.adapter.UsersAdapter
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment :
    BaseFragment<FragmentHomeBinding, HomeViewModel>(),
    HomeNavigator, UsersAdapter.OnItemClickListener {

    private val viewModels: HomeViewModel by viewModel()

    override fun getViewModel(): HomeViewModel = viewModels

    override fun getLayoutId(): Int = R.layout.fragment_home

    override fun getBindingVariable(): Int = BR.model

    private lateinit var usersAdapter: UsersAdapter

    private var usersListResponse = mutableListOf<UsersResponse>()

    private var mArguments: TemplateModel? = null

    private var isInit = false

    override fun onResume() {
        super.onResume()
//        viewModels.getUsers()
    }

    override fun updateInstant(savedInstanceState: Bundle?) {
        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        // Receiver argument
        // arguments?.let {
        //     credentialsDao = LoginFragmentArgs.fromBundle(it).model
        // }
    }

    override fun observerLiveData() {
        lifecycleScope.launchWhenStarted {
            viewModels.usersResponse.collect { result ->
                when (result) {
                    is Success -> {
                        result.data?.let {
                            usersListResponse = it
                            setUsersListData(data = it)
                        }
                    }
                    is Failure -> {
                    }
                    is Loading -> {
                    }
                }
            }
        }
    }

    override fun updateListener() {
//        btnBack.clickWithDebounce {
//            goBackToPrevious()
//        }
//        binding?.data = mArguments
    }

    override fun updateUI(savedInstanceState: Bundle?) {
        viewModels.setNavigator(this)
        binding?.viewModel = viewModels
        initRecyclerView()
        viewModels.getUsers()
    }

    private fun initRecyclerView() {
        usersAdapter = UsersAdapter()
        binding?.recyclerView?.apply {
            setUpRcv(
                rcv = this,
                adapter = usersAdapter,
                orientation = RecyclerView.VERTICAL,
                colorDivider = R.color.colorBlack,
                showLastDivider = true
            )
        }.run {
            usersAdapter.callback = this@HomeFragment
        }
    }

    private fun setUsersListData(data: MutableList<UsersResponse>) {
        usersAdapter.usersListResponse = data
        usersAdapter.notifyDataSetChanged()
    }

    override fun navigateToDetail(id: String) {
        navController.navigate(
            HomeFragmentDirections.actionHomeFragmentToUserDetailFragment(
                userFragmentBundle = UserFragmentBundle()
            )
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun clearKeyboard() {
    }

    override fun onBackPressActivityListener() {
        requireActivity().finish()
    }

    override fun goBackToPrevious() {
        navController.popBackStack()
    }

    override fun onItemClick(position: Int) {
        toast("email: ${usersListResponse[position].email}")
        navigateToDetail(usersListResponse[position].userId.toString())
    }

}
