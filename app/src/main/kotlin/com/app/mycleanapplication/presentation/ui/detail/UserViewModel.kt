package com.app.mycleanapplication.presentation.ui.detail

import androidx.lifecycle.viewModelScope
import com.app.mycleanapplication.base.BaseViewModel
import com.app.mycleanapplication.domain.Result
import com.app.mycleanapplication.domain.Result.Failure
import com.app.mycleanapplication.domain.Result.Loading
import com.app.mycleanapplication.domain.model.users.UsersResponse
import com.app.mycleanapplication.domain.usecase.home.GetUserUseCase
import com.app.mycleanapplication.exception.NoConnectivityException
import com.app.mycleanapplication.utils.retryIO
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

/**
 * Created by chonlakan.s on 10:13 2018-12-19
 */

class UserViewModel(
    private val getUserUseCase: GetUserUseCase
) : BaseViewModel<UserNavigator>() {

    private val _usersResponse =
        MutableStateFlow<Result<UsersResponse>>(Loading(isLoad = true))
    val usersResponse: StateFlow<Result<UsersResponse>> = _usersResponse

    private val _localResponse =
        MutableStateFlow<Result<UsersResponse>>(Loading(isLoad = true))
    val localResponse: StateFlow<Result<UsersResponse>> = _localResponse

//    private val _mainResponse = MutableLiveData<Result<BaseResponse<Nothing>>>()
//    val mainResponse: LiveData<Result<BaseResponse<Nothing>>> = _mainResponse

    fun getUsersById(id: String) {
        viewModelScope.launch {
            try {
                retryIO(times = 3) {
                    getUserUseCase(id = id).onStart {
                        _usersResponse.value = Loading(true)
                    }.collect {
                        _usersResponse.value = it
                    }
                }
            } catch (e: NoConnectivityException) {
                _usersResponse.value = Failure(e.message)
            } catch (e: Exception) {
                _usersResponse.value = Failure(e.message)
            }
        }
    }

//    fun getHomeLocal(getHomeRequest: GetHomeRequest) {
//        viewModelScope.launch {
//            try {
//                retryIO(times = 3) {
//                    getHomeUseCase.getHomeLocal(getHomeRequest).onStart {
//                        _localResponse.value = Loading(true)
//                    }.collect {
//                        _localResponse.value = it
//                    }
//                }
//            } catch (e: NoConnectivityException) {
//                _localResponse.value = Failure(e.message)
//            } catch (e: Exception) {
//                _localResponse.value = Failure(e.message)
//            }
//        }
//    }
}
