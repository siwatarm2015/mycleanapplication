package com.app.mycleanapplication.presentation.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.mycleanapplication.R
import com.app.mycleanapplication.base.BaseViewHolder
import com.app.mycleanapplication.databinding.ItemUserBinding
import com.app.mycleanapplication.domain.model.users.UsersResponse

class UsersAdapter : RecyclerView.Adapter<BaseViewHolder<*>>() {

//    var items: List<MapDataEvenModel> by Delegates.observable(emptyList()) { prop, old, new ->
//        autoNotify(
//            old,
//            new
//        ) { o, n ->
//            n.isTypeYear == o.isTypeYear && n.event.id == o.event.id
//        }
//    }
    lateinit var callback: OnItemClickListener
    var usersListResponse: MutableList<UsersResponse>? = null

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ItemUserBinding =
            DataBindingUtil.inflate(inflater, R.layout.item_user, parent, false)
        return UsersAdapterViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        return when (holder) {
            is UsersAdapterViewHolder -> {
                holder.bind(position)
            }
            else -> {
                throw IllegalArgumentException()
            }
        }
    }

    override fun getItemCount(): Int = usersListResponse?.size ?: 0

    inner class UsersAdapterViewHolder(binding: ItemUserBinding) :
        BaseViewHolder<ItemUserBinding>(binding = binding) {
        fun bind(position: Int) {
            binding.adapter = this
            binding.position = position
            binding.model = usersListResponse?.get(position)
            binding.executePendingBindings()
        }

        fun onItemClick(position: Int){
            callback.onItemClick(position)
        }

    }
}