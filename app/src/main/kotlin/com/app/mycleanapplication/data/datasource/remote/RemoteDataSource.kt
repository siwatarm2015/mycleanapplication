package com.app.mycleanapplication.data.datasource.remote

import com.app.mycleanapplication.data.datasource.DataSource
import com.app.mycleanapplication.data.service.ApiService
import com.app.mycleanapplication.domain.Result
import com.app.mycleanapplication.domain.Result.Failure
import com.app.mycleanapplication.domain.Result.Success
import com.app.mycleanapplication.domain.dao.UserDao
import com.app.mycleanapplication.domain.model.users.UsersResponse
import com.app.mycleanapplication.exception.NoConnectivityException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.retryWhen
import kotlinx.coroutines.withContext

class RemoteDataSource(
    private val apiService: ApiService,
    private val userDao: UserDao
) : DataSource {

    override suspend fun getUsers(
    ): Flow<Result<MutableList<UsersResponse>>> {
        return flow {
            val response = apiService.getUsers()
            try {
                if (response.isSuccessful) {
                    response.body()?.let {
                        withContext(Dispatchers.IO) { userDao.add(users = it) }
                    }
                    emit(Success(response.body()))
                }else{
                    emit(Failure(response.message()))
                }
            } catch (throwable: NoConnectivityException) {
                emit(Failure(throwable.message))
            }
        }.retryWhen { cause, attempt ->
            when {
                cause is NoConnectivityException && attempt < 3 -> {
                    delay(2000)
                    true
                }
                else -> {
                    false
                }
            }

        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getUsersById(id: String): Flow<Result<UsersResponse>> {
        return flow {
            val response = apiService.getUsersById(
                id = id
            )
            try {
                emit(Success(response.body()))
            } catch (throwable: NoConnectivityException) {
                emit(Failure(throwable.message))
            }
        }.retryWhen { cause, attempt ->
            when {
                cause is NoConnectivityException && attempt < 3 -> {
                    delay(2000)
                    true
                }
                else -> {
                    false
                }
            }

        }.flowOn(Dispatchers.IO)
    }
}

