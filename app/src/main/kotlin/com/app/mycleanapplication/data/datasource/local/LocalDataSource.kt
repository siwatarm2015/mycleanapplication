package com.app.mycleanapplication.data.datasource.local

import com.app.mycleanapplication.data.datasource.DataSource
import com.app.mycleanapplication.data.datasource.local.db.DatabaseManager
import com.app.mycleanapplication.domain.Result
import com.app.mycleanapplication.domain.Result.Failure
import com.app.mycleanapplication.domain.Result.Success
import com.app.mycleanapplication.domain.model.users.UsersResponse
import com.app.mycleanapplication.exception.NoConnectivityException
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

class LocalDataSource(
    private val databaseManager: DatabaseManager
) : DataSource {
    override suspend fun getUsers(): Flow<Result<MutableList<UsersResponse>>> {
        return flow {
            databaseManager.userDao.getUserListFromCache().collect {
                try {
                    if (it.isNotEmpty()) {
//                        val list = arrayListOf<UsersResponse>()
//                        list.addAll(it)
                        emit(Success(it))
                    } else {
                        emit(Failure("data not found in room"))
                    }
                } catch (throwable: NoConnectivityException) {
                    emit(Failure(throwable.message))
                }
            }
        }

    }

    override suspend fun getUsersById(id: String): Flow<Result<UsersResponse>> {
        return flow {
            Success(null)
        }
    }
}

