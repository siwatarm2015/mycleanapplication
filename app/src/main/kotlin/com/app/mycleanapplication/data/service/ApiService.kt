package com.app.mycleanapplication.data.service

import com.app.mycleanapplication.domain.BaseResponse
import com.app.mycleanapplication.domain.model.request.GetHomeRequest
import com.app.mycleanapplication.domain.model.users.UsersResponse
import retrofit2.Response
import retrofit2.http.*

/**
 * REST API access points
 */

interface ApiService {

    @GET("main")
    suspend fun getHome(
        @Header("Authorization") token: String,
        @Query("longitude") longitude: Double,
        @Query("latitude") latitude: Double,
        @Query("radius") radius: Int
    ): Response<BaseResponse<Nothing>>

    @GET("main")
    suspend fun getHome(
        @Header("Authorization") token: String,
        @Body getHomeRequest: GetHomeRequest
    ): Response<BaseResponse<Nothing>>

    @GET("users")
    suspend fun getUsers(): Response<MutableList<UsersResponse>>

    @GET("users/{id}")
    suspend fun getUsersById(
        @Path("id") id: String
    ): Response<UsersResponse>

}
