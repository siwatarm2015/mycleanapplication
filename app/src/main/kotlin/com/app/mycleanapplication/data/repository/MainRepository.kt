package com.app.mycleanapplication.data.repository

import com.app.mycleanapplication.data.datasource.local.LocalDataSource
import com.app.mycleanapplication.data.datasource.remote.RemoteDataSource
import com.app.mycleanapplication.domain.Result
import com.app.mycleanapplication.domain.model.users.UsersResponse
import kotlinx.coroutines.flow.Flow

class MainRepository(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource
) {
    suspend fun getUsers(networkOnly: Boolean = true): Flow<Result<MutableList<UsersResponse>>> {
        if (!networkOnly) {
            return localDataSource.getUsers()
        }
        return remoteDataSource.getUsers()
    }

    suspend fun getUsersById(
        id: String,
        networkOnly: Boolean = true
    ): Flow<Result<UsersResponse>> {
        if (!networkOnly) {
            return localDataSource.getUsersById(
                id = id
            )
        }
        return remoteDataSource.getUsersById(
            id = id
        )
    }
}
