package com.app.mycleanapplication.data.datasource

import com.app.mycleanapplication.domain.Result
import com.app.mycleanapplication.domain.model.users.UsersResponse
import kotlinx.coroutines.flow.Flow

interface DataSource {
    suspend fun getUsers(
    ): Flow<Result<MutableList<UsersResponse>>>

    suspend fun getUsersById(id: String
    ): Flow<Result<UsersResponse>>
}

