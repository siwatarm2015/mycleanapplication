package com.app.mycleanapplication.data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.app.mycleanapplication.utils.TestCoroutineRule
import com.app.mycleanapplication.data.datasource.local.LocalDataSource
import com.app.mycleanapplication.data.datasource.remote.RemoteDataSource
import com.app.mycleanapplication.domain.BaseResponse
import com.app.mycleanapplication.domain.Result.Success
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import org.junit.Assert.assertNotNull
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MainRepositoryTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val remoteDataSource = mock(RemoteDataSource::class.java)

    private val localDataSource = mock(LocalDataSource::class.java)

    private val mainRepo = MainRepository(remoteDataSource, localDataSource)

    @Test
    fun `getUsers - success`() {
        testCoroutineRule.runBlockingTest {
            `when`(
                remoteDataSource.getUsers()
            ).thenReturn(
                flow {
                    Success(
                        data = BaseResponse(
                            msg = "Success",
                            statusCode = 200,
                            error = "",
                            response = null
                        )
                    )
                }
            )
            mainRepo.getUsers().collect { result ->
                result as Success
                verify(remoteDataSource, times(1)).getUsers()
                assertNotNull(result.data)
            }
        }
    }

    @Test
    fun `getUsers - failed`() {
        testCoroutineRule.runBlockingTest {
            `when`(
                remoteDataSource.getUsers()
            ).thenReturn(
                flow {
                    Success(
                        data = BaseResponse(
                            msg = "Success",
                            statusCode = 200,
                            error = "",
                            response = null
                        )
                    )
                }
            )
            mainRepo.getUsers().collect { result ->
                result as Success
                verify(remoteDataSource, times(1)).getUsers()
                assertNotNull(result.data)
            }
        }
    }
}


