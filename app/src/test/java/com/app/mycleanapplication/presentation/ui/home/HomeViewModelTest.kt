package com.app.mycleanapplication.presentation.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.app.mycleanapplication.utils.TestCoroutineRule
import com.app.mycleanapplication.domain.Result.*
import com.app.mycleanapplication.domain.model.users.UsersResponse
import com.app.mycleanapplication.domain.usecase.home.GetUserUseCase
import com.app.mycleanapplication.utils.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class HomeViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var getUserUseCase: GetUserUseCase

    private lateinit var homeViewModel: HomeViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        getUserUseCase = Mockito.mock(GetUserUseCase::class.java)
        homeViewModel = HomeViewModel(getUserUseCase)
    }

    @Test
    fun getUser() {
        runBlockingTest {
            val result = mock<MutableStateFlow<Result<MutableList<UsersResponse>>>>()
            homeViewModel.usersResponse.map { result }
            homeViewModel.getUsers()
            result.collect { data ->
                Success(data)
                verify(data) {
                    Success(it)
                }
            }
        }
    }
}