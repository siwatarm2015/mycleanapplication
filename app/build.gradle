import java.nio.file.Paths

apply plugin: 'com.android.application'
apply plugin: 'kotlin-android'
apply plugin: 'kotlin-android-extensions'
apply plugin: 'kotlin-kapt'
apply plugin: "androidx.navigation.safeargs.kotlin"

/*
* Automates generation of Release APK
* ./gradlew assembleRelease
* */
Properties props = new Properties()
def userHome = Paths.get(System.getProperty('user.home'))
def propFile = file(userHome.resolve('gradle.properties'))
if (propFile.canRead()) {
    props.load(new FileInputStream(propFile))
    if (props != null && props.containsKey('signingConfigs_storeFile') && props.containsKey('signingConfigs_storePassword')
            && props.containsKey('signingConfigs_keyAlias') && props.containsKey('signingConfigs_keyPassword')) {
        android.signingConfigs.release.storeFile = file(props['signingConfigs_storeFile'])
        android.signingConfigs.release.storePassword = props['signingConfigs_storePassword']
        android.signingConfigs.release.keyAlias = props['signingConfigs_keyAlias']
        android.signingConfigs.release.keyPassword = props['signingConfigs_keyPassword']
    } else {
        println 'signing.properties found but some entries are missing'
        android.buildTypes.release.signingConfig = null
    }
} else {
    println 'signing.properties not found'
    android.buildTypes.release.signingConfig = null
}

def libs = rootProject.ext.libraries
def config = rootProject.ext.configuration

android {
    compileSdkVersion 30
    buildToolsVersion "30.0.2"

    defaultConfig {

        kapt {
            arguments {
                arg("room.schemaLocation", "$projectDir/schemas".toString())
                arg("room.incremental", "true")
                arg("room.expandProjection", "true")
            }
        }

        applicationId config.package
        minSdkVersion config.minSdkVersion
        targetSdkVersion config.targetSdkVersion
        multiDexEnabled true
        versionCode config.versionCode
        versionName config.versionName
        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables.useSupportLibrary = true
    }

    buildFeatures {
        dataBinding true
        viewBinding true
    }

    androidExtensions {
        experimental = true
    }

    signingConfigs {
        debug {
            keyAlias signingConfigs_keyAlias
            keyPassword signingConfigs_keyPassword
            storeFile file(signingConfigs_storeFile)
            storePassword signingConfigs_storePassword
        }

        release {
            keyAlias signingConfigs_keyAlias
            keyPassword signingConfigs_keyPassword
            storeFile file(signingConfigs_storeFile)
            storePassword signingConfigs_storePassword
        }
    }

    buildTypes {
        debug {
//            ext.enableCrashlytics = false
            ext.alwaysUpdateBuildId = false
            ext.enableCrashlytics = false
            ext.enableStability = false
//            FirebasePerformance {
//                // Set this flag to 'false' to disable @AddTrace annotation processing and
//                // automatic HTTP/S network request monitoring
//                // for a specific build variant at compile time.
//                instrumentationEnabled false
//            }
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
            debuggable true
//            testProguardFile 'proguard-test-rules.pro'
        }

        release {
            minifyEnabled true
            shrinkResources true
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
            signingConfig signingConfigs.release
        }
    }

    /*
* To let gradle automate signing process, lintOptions to be added
* */
    lintOptions {
        checkReleaseBuilds false
        abortOnError false
    }


    flavorDimensions "api"
    productFlavors {
        prod {
            dimension "api"
            applicationId "com.app.mycleanapplication"
            buildConfigField 'String', 'BASE_URL', '"https://jsonplaceholder.typicode.com/"'
        }
        dev {
            dimension "api"
            applicationId "com.app.mycleanapplication"
            versionNameSuffix "-dev"
            buildConfigField 'String', 'BASE_URL', '"https://jsonplaceholder.typicode.com/"'
        }
        uat {
            dimension "api"
            applicationId "com.app.mycleanapplication"
            versionNameSuffix "-uat"
            buildConfigField 'String', 'BASE_URL', '"https://jsonplaceholder.typicode.com/"'
        }

    }

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = '1.8'
    }

    repositories {
        flatDir {
            dirs 'libs'
        }
    }

    sourceSets {
        main.java.srcDirs += 'src/main/kotlin'
        debug.java.srcDirs += 'src/debug/kotlin'
        release.java.srcDirs += 'src/release/kotlin'
        test.java.srcDirs += 'src/test/kotlin'
        dev.java.srcDirs += 'src/dev/kotlin'
        prod.java.srcDirs += 'src/prod/kotlin'
    }

    applicationVariants.all { variant ->
        variant.outputs.all { output ->
            def formattedDate = new Date().format('yy-MM-dd')
            def flavor = variant.name
            def versionName = variant.versionName
            def versionCode = variant.versionCode
            outputFileName = "app_v${versionName}_b_${versionCode}_${flavor.toLowerCase().replace("release", "")}_${formattedDate}.apk"
        }
    }


    bundle {
        abi {
            enableSplit = false
        }
        density {
            enableSplit = false
        }
        language {
            enableSplit = false
        }
    }
}

dependencies {
    implementation fileTree(dir: "libs", include: ["*.jar"])
//    implementation "org.jetbrains.kotlin:kotlin-stdlib:$kotlin_version"
    implementation "androidx.core:core-ktx:$libs.core_ktx"
    implementation "androidx.appcompat:appcompat:$libs.appcompat"
    implementation "com.google.android.material:material:$libs.material"
    implementation "androidx.constraintlayout:constraintlayout:$libs.constraintlayout"
    implementation "androidx.navigation:navigation-fragment-ktx:$libs.navigation_fragment_ktx"
    implementation "androidx.navigation:navigation-ui-ktx:$libs.navigation_ui_ktx"
    testImplementation "org.jetbrains.kotlinx:kotlinx-coroutines-test:$libs.kotlinx_coroutines_test"
    testImplementation "io.mockk:mockk:$libs.mockk"
    testImplementation "org.mockito:mockito-core:$libs.mockito_core"
    testImplementation "androidx.arch.core:core-testing:$libs.core_testing"
    testImplementation "org.mockito:mockito-inline:$libs.mockito_inline"
    testImplementation "junit:junit:$libs.junit"
    androidTestImplementation "androidx.test.ext:junit:$libs.ext_junit"
    androidTestImplementation "androidx.test.espresso:espresso-core:$libs.espresso_core"

    // Room components
    implementation "androidx.room:room-ktx:$libs.roomVersion"
    kapt "androidx.room:room-compiler:$libs.roomVersion"
    androidTestImplementation "androidx.room:room-testing:$libs.roomVersion"

    //kotlin coroutine
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:$libs.kotlinx_coroutines_core"
    implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:$libs.lifecycle_viewmodel_ktx"

    //Image loader
    implementation "io.coil-kt:coil:$libs.coil"
    implementation "com.github.bumptech.glide:glide:$libs.glide"
    implementation("com.github.Commit451.coil-transformations:transformations:${libs.coilTransform}")
    kapt "com.github.bumptech.glide:compiler:$libs.glide"

    //network
    implementation "com.squareup.retrofit2:retrofit:$libs.retrofit"
    implementation "com.squareup.retrofit2:converter-gson:$libs.gson_converter"

    // Koin for Android
    implementation "org.koin:koin-android:$libs.koin_version"
    // or Koin for Lifecycle scoping
    implementation "org.koin:koin-androidx-scope:$libs.koin_version"
    // or Koin for Android Architecture ViewModel
    implementation "org.koin:koin-androidx-viewmodel:$libs.koin_version"
    //hawk
    implementation "com.orhanobut:hawk:$libs.hawk"
    //Timber Log
    implementation "com.jakewharton.timber:timber:$libs.timber"
    implementation "com.orhanobut:logger:$libs.logger"

    implementation "me.jessyan:autosize:$libs.auto_size"

    //RecyclerView
    implementation "com.yqritc:recyclerview-flexibledivider:${libs.flexibledivider}"
    implementation "androidx.recyclerview:recyclerview:$libs.recyclerView"

    // OkHttp
    implementation "com.squareup.okhttp3:okhttp:$libs.okhttp"
    implementation "com.squareup.okhttp3:logging-interceptor:$libs.loggingInterceptor"

    //chuck
    debugImplementation "com.readystatesoftware.chuck:library:$libs.chuck"
    releaseImplementation "com.readystatesoftware.chuck:library-no-op:$libs.chuck"

    //RxPermission
    implementation "com.github.tbruyelle:rxpermissions:$libs.rxPermission"
    implementation "com.karumi:dexter:$libs.dexter"

    //editText validation
    implementation "com.github.thomhurst:Android-EditText-Validations:${libs.validations}"
    implementation "com.github.rubensousa:gravitysnaphelper:$libs.gravitysnaphelper"

    implementation "androidx.biometric:biometric:${libs.biometric}"

    //EmptyView
    implementation "com.github.santalu:emptyview:$libs.emptyView"

    // RxKotlin & RxAndroid
    implementation "io.reactivex.rxjava3:rxkotlin:${libs.rxkotlin}"
    implementation "io.reactivex.rxjava3:rxandroid:${libs.rxandroid}"
    implementation "io.reactivex.rxjava3:rxjava:${libs.rxjava}"
    implementation "com.jakewharton.rxrelay2:rxrelay:${libs.rxrelay}"
    implementation "com.jakewharton.rxbinding4:rxbinding:${libs.rxbinding4}"
//    implementation 'com.jakewharton.rxbinding4:rxbinding-core:4.0.0'
//    implementation 'com.jakewharton.rxbinding4:rxbinding-appcompat:4.0.0'
//    implementation 'com.jakewharton.rxbinding4:rxbinding-drawerlayout:4.0.0'
//    implementation 'com.jakewharton.rxbinding4:rxbinding-leanback:4.0.0'
//    implementation 'com.jakewharton.rxbinding4:rxbinding-recyclerview:4.0.0'
//    implementation 'com.jakewharton.rxbinding4:rxbinding-slidingpanelayout:4.0.0'
//    implementation 'com.jakewharton.rxbinding4:rxbinding-swiperefreshlayout:4.0.0'
//    implementation 'com.jakewharton.rxbinding4:rxbinding-viewpager:4.0.0'
//    implementation 'com.jakewharton.rxbinding4:rxbinding-viewpager2:4.0.0'
}